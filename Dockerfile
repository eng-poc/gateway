FROM openjdk:11-jdk
COPY target/*.jar gateway-proxy.jar
ENTRYPOINT ["java", "-jar", "/gateway-proxy.jar"]
EXPOSE 8080